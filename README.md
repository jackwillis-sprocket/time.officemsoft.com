# time.officemsoft.com

The source code for https://time.officemsoft.com

The program displays the current time for UTC, Chicago, and São Paulo.
It also displays the user's public IP address and HTTP headers.

It supports English, Portuguese, Spanish, and French.

This is a static web page with no build process and no backend. It deploys to Netlify.

It uses the following APIs:

- [ipify](https://www.ipify.org/) for the public IP address
- [httpbin.org](https://httpbin.org) for the HTTP headers